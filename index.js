const express = require('express');
const app = express();
const port = 3000;

app.get('/health', (req, res) => {
    res.send("Server is up");
})

app.listen(port, () => {
    console.log(`Server is listening to port ${port}`);
})